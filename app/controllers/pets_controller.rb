class PetsController < ApplicationController
  before_action :signed_in_user


  def create
    @pet = current_user.build_pet(pet_params)
    if @pet.save
      flash[:success] = "Seu pet está pronto!"
      redirect_to root_url
    else
      render 'static_pages/home'
    end
  end

  def destroy
  end

  private

    def pet_params
      params.require(:pet).permit(:nome)
    end
end
