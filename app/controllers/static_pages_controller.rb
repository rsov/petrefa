class StaticPagesController < ApplicationController
  def home
    if signed_in?
      @tarefa = current_user.tarefas.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  def help
    @pet = current_user.build_pet if signed_in?
  end

  def about
  end
end
