class TarefasController < ApplicationController
  before_action :signed_in_user
  before_action :correct_user,   only: :destroy

  def create
    @tarefa = current_user.tarefas.build(tarefa_params)
    if @tarefa.save
      flash[:success] = "Tarefa criada!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def destroy
    @tarefa.destroy
    redirect_to root_url
  end

  private

    def tarefa_params
      params.require(:tarefa).permit(:descricao, :prazo)
    end

    def correct_user
      @tarefa = current_user.tarefas.find_by(id: params[:id])
      redirect_to root_url if @tarefa.nil?
    end
end
