class Pet < ActiveRecord::Base
  belongs_to :user
  validates :user_id, presence: true
  validates :nome, presence: true, length: { maximum: 15 }
end
