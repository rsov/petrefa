class Tarefa < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order('created_at DESC') }
  validates :user_id, presence: true
  validates :descricao, presence: true, length: { maximum: 70 }
end
