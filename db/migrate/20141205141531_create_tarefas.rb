class CreateTarefas < ActiveRecord::Migration
  def change
    create_table :tarefas do |t|
      t.string :descricao
      t.integer :user_id
      t.integer :categoria
      t.integer :dificuldade
      t.integer :cor
      t.date :prazo

      t.timestamps
    end
    add_index :tarefas, [:user_id, :created_at]
  end
end
