class ChangeDataTypeForPrazo < ActiveRecord::Migration
  def self.up
    change_table :tarefas do |t|
      t.change :prazo, :string
    end
  end
  def self.down
    change_table :tarefas do |t|
      t.change :prazo, :date
    end
  end
end
