class CreatePets < ActiveRecord::Migration
  def change
    create_table :pets do |t|
      t.integer :user_id
      t.integer :especie
      t.string :nome
      t.integer :felicidade
      t.integer :alimentacao
      t.integer :inteligencia
      t.integer :cor
      t.integer :custom

      t.timestamps
    end
  end
end
