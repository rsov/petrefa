class ChangePrazoTypeinTarefas < ActiveRecord::Migration
  def self.up
    change_column :tarefas, :prazo, :date
  end

  def self.down
change_column :tarefas, :prazo, :string
  end
end
