FactoryGirl.define do
  factory :user do
    sequence(:name)  { |n| "Person #{n}" }
    sequence(:email) { |n| "person_#{n}@example.com"}
    password "foobar"
    password_confirmation "foobar"

    factory :admin do
      admin true
    end
  end

  factory :tarefa do
    descricao "Lorem ipsum"
    dificuldade 1
    categoria 1
    cor 1
    prazo 2015-01-01
    user
  end

  factory :pet do
    especie 1
    nome "Gersonrefa"
    felicidade 10
    alimentacao 10
    inteligencia 10
    cor 1
    custom 1
    user
  end
end
