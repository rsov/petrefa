require 'spec_helper'

describe Pet do

  let(:user) { FactoryGirl.create(:user) }
  before { @pet = user.build_pet(especie: 1, user_id: user.id,
                  nome: "Gersonrefa", felicidade: 5, alimentacao: 5,
                  inteligencia: 5, cor: 1, custom: 1) }


  subject { @pet }

  it { should respond_to(:especie) }
  it { should respond_to(:user_id) }
  it { should respond_to(:nome) }
  it { should respond_to(:felicidade) }
  it { should respond_to(:alimentacao) }
  it { should respond_to(:inteligencia) }
  it { should respond_to(:cor) }
  it { should respond_to(:custom) }
  it { should respond_to(:user) }
  its(:user) { should eq user }

  describe "when user_id is not present" do
    before { @pet.user_id = nil }
    it { should_not be_valid }
  end

  describe "with blank name" do
    before { @pet.nome = " " }
    it { should_not be_valid }
  end

  describe "with name that is too long" do
    before { @pet.nome = "a" * 16 }
    it { should_not be_valid }
  end
end
