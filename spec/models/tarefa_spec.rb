require 'spec_helper'

describe Tarefa do

  let(:user) { FactoryGirl.create(:user) }
  before { @tarefa = user.tarefas.build(descricao: "Lorem ipsum") }

  subject { @tarefa }

  it { should respond_to(:descricao) }
  it { should respond_to(:dificuldade) }
  it { should respond_to(:categoria) }
  it { should respond_to(:cor) }
  it { should respond_to(:prazo) }
  it { should respond_to(:user_id) }
  it { should respond_to(:user) }
  its(:user) { should eq user }

  it { should be_valid }

  describe "when user_id is not present" do
    before { @tarefa.user_id = nil }
    it { should_not be_valid }
  end

  describe "with blank content" do
    before { @tarefa.descricao = " " }
    it { should_not be_valid }
  end

  describe "with content that is too long" do
    before { @tarefa.descricao = "a" * 71 }
    it { should_not be_valid }
  end
end
