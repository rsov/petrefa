require 'spec_helper'

describe "Authentication" do

  subject { page }

  describe "login page" do
    before { visit login_path }

    it { should have_content('Login') }
    it { should have_title('Login') }
  end

  describe "login" do
    before { visit login_path }

    describe "with invalid information" do
      before { click_button "Login" }

      it { should have_title('Login') }
      it { should have_selector('div.alert.alert-error') }

      describe "after visiting another page" do
        before { click_link "petRefa" }
        it { should_not have_selector('div.alert.alert-error') }
      end
    end

    describe "with valid information" do
      let(:user) { FactoryGirl.create(:user) }
      before do
        fill_in "Email",    with: user.email.upcase
        fill_in "Senha", with: user.password
        click_button "Login"
      end

      it { should have_title("petRefa temporario") }
      it { should_not have_link('Usuarios',       href: users_path) }
      it { should have_link('Ajustes',     href: edit_user_path(user)) }
      it { should have_link('Logout',    href: logout_path) }
      it { should_not have_link('Login', href: login_path) }

      describe "followed by signout" do
        before { click_link "Logout" }
        it { should have_link('Login') }
      end
    end
  end

  describe "authorization" do

    describe "for non-signed-in users" do
      let(:user) { FactoryGirl.create(:user) }
      before { FactoryGirl.create(:pet, user: user, nome: "Ruirefa") }

      describe "when attempting to visit a protected page" do
        before do
          visit edit_user_path(user)
          fill_in "Email",    with: user.email
          fill_in "Senha", with: user.password
          click_button "Login"
        end

        describe "after signing in" do

          it "should render the desired protected page" do
            expect(page).to have_title('Ajustes')
          end
        end
      end

      describe "in the Tarefas controller" do

        describe "submitting to the create action" do
          before { post tarefas_path }
          specify { expect(response).to redirect_to(login_path) }
        end

        describe "submitting to the destroy action" do
          before { delete tarefa_path(FactoryGirl.create(:tarefa)) }
          specify { expect(response).to redirect_to(login_path) }
        end
      end

      # describe "in the Pets controller" do

      #   describe "submitting to the create action" do
      #     before { post pets_path }
      #     specify { expect(response).to redirect_to(login_path) }
      #   end

      #   describe "submitting to the destroy action" do
      #     before { delete pet_path(FactoryGirl.create(:pet)) }
      #     specify { expect(response).to redirect_to(login_path) }
      #   end
      # end

      describe "in the Users controller" do

        describe "visiting the edit page" do
          before { visit edit_user_path(user) }
          it { should have_title('Login') }
        end

        describe "submitting to the update action" do
          before { patch user_path(user) }
          specify { expect(response).to redirect_to(login_path) }
        end

        describe "visiting the user index" do
          before { visit users_path }
          it { should have_title('Login') }
        end
      end
    end

    describe "as wrong user" do
      let(:user) { FactoryGirl.create(:user) }
      let(:wrong_user) { FactoryGirl.create(:user, email: "wrong@example.com") }
      before { login user, no_capybara: true }

      describe "submitting a GET request to the Users#edit action" do
        before { get edit_user_path(wrong_user) }
        specify { expect(response.body).not_to match(full_title('Ajustes')) }
        specify { expect(response).to redirect_to(root_url) }
      end

      describe "submitting a PATCH request to the Users#update action" do
        before { patch user_path(wrong_user) }
        specify { expect(response).to redirect_to(root_url) }
      end
    end

    describe "as non-admin user" do
      let(:user) { FactoryGirl.create(:user) }
      let(:non_admin) { FactoryGirl.create(:user) }

      before { login non_admin, no_capybara: true }

      describe "submitting a DELETE request to the Users#destroy action" do
        before { delete user_path(user) }
        specify { expect(response).to redirect_to(root_url) }
      end
    end
  end
end
