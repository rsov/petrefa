require 'spec_helper'

describe "Static pages" do
  let(:base_title) { "petRefa temporario |" }

  describe "Home page" do
    before { visit root_path }

    it "should have the content 'petRefa'" do
      expect(page).to have_content('petRefa')
    end

    it "should have the base title" do
      expect(page).to have_title("petRefa temporario")
    end

    it "should not have a custom page title" do
      expect(page).not_to have_title('| Home')
    end

    describe "for signed-in users" do
      let(:user) { FactoryGirl.create(:user) }
      before do
        FactoryGirl.create(:tarefa, user: user, descricao: "Lorem ipsum")
        FactoryGirl.create(:tarefa, user: user, descricao: "Dolor sit amet")
        login user
        visit root_path
      end

      it "should render the user's feed" do
        user.feed.each do |item|
          expect(page).to have_selector("li##{item.id}", text: item.descricao)
        end
      end
    end
  end

  describe "Help page" do
    before { visit ajuda_path}

    it "should have the content 'Ajuda'" do
      expect(page).to have_selector('h1', text: 'Ajuda')
    end

    it "should have the right title" do
      expect(page).to have_title("#{base_title} Ajuda")
    end
  end

  describe "About page" do
    before { visit desenvolvedores_path }

    it "should have the content 'Desenvolvedores'" do
      expect(page).to have_content('Desenvolvedores')
    end

    it "should have the right title" do
      expect(page).to have_title("#{base_title} Desenvolvedores")
    end
  end

  it "should have the right links on the layout" do
    visit root_path
    click_link "Desenvolvedores"
    expect(page).to have_title('Desenvolvedores')
    click_link "Cadastrar"
    expect(page).to have_title('Cadastro')
    click_link "Ajuda"
    expect(page).to have_title('Ajuda')
    click_link "petRefa"
    click_link "Crie uma conta!"
    expect(page).to have_title('Cadastro')
  end
end
