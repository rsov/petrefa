require 'spec_helper'

describe "Tarefa pages" do

  subject { page }

  let(:user) { FactoryGirl.create(:user) }
  before { login user }

  describe "tarefa creation" do
    before { visit root_path }

    describe "with invalid information" do

      it "should not create a tarefa" do
        expect { click_button "Criar Tarefa" }.not_to change(Tarefa, :count)
      end

      describe "error messages" do
        before { click_button "Criar Tarefa" }
        it { should have_content('erro') }
      end
    end

    describe "with valid information" do

      before { fill_in 'tarefa_descricao', with: "Lorem ipsum" }
      it "should create a tarefa" do
        expect { click_button "Criar Tarefa" }.to change(Tarefa, :count).by(1)
      end
    end
  end

  describe "tarefa destruction" do
    before { FactoryGirl.create(:tarefa, user: user) }

    describe "as correct user" do
      before { visit root_path }

      it "should delete a tarefa" do
        expect { click_link "Feito!" }.to change(Tarefa, :count).by(-1)
      end
    end
  end
end
