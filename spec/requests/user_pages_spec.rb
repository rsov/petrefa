require 'spec_helper'

describe "User pages" do

  describe "index" do
    let(:user) { FactoryGirl.create(:user) }
    before(:each) do
      login user
      visit users_path
    end

    specify{ expect(page).not_to have_content('Todos os Usuarios') }
    specify{ expect(page).to have_title('Todos os Usuarios') }

    describe "as admin" do
      let(:user) { FactoryGirl.create(:admin) }
      before do
      login user
      visit users_path
      end

      specify{ expect(page).to have_content('Todos os Usuarios') }
      specify{ expect(page).to have_title('Todos os Usuarios') }

      describe "pagination" do

        before(:all) { 30.times { FactoryGirl.create(:user) } }
        after(:all)  { User.delete_all }

       specify{ expect(page).to have_selector('div.pagination') }

       it "should list each user" do
          User.paginate(page: 1).each do |user|
            expect(page).to have_selector('li', text: user.name)
          end
        end
      end
    end

    describe "delete links" do

      describe "as an admin user" do
        let(:admin) { FactoryGirl.create(:admin) }
        before do
          login admin
          visit users_path
        end

        it "should list each user" do
          User.all.each do |user|
          expect(page).to have_selector('li', text: user.name)
          end
        end
        specify { expect(page).to have_link('deletar',
                                            href: user_path(User.first)) }
        it "should be able to delete another user" do
          expect do
            click_link('deletar', match: :first)
          end.to change(User, :count).by(-1)
        end
        specify { expect(page).not_to have_link('deletar',
                                            href: user_path(admin)) }
        #it { should_not have_link('deletar', href: user_path(admin)) }
      end
    end
  end

  describe "profile page" do
    let(:user) { FactoryGirl.create(:user) }
    before do
      login user
      FactoryGirl.create(:tarefa, user: user, descricao: "Foo")
      FactoryGirl.create(:tarefa, user: user, descricao: "Bar")
      FactoryGirl.create(:pet, user: user, nome: "Ruirefa")
      visit user_path(user)
    end

    specify{ expect(page).to have_title(user.name) }

    describe "tarefas" do
      specify{ expect(page).to have_content("Foo") }
      specify{ expect(page).to have_content("Bar") }
      specify{ expect(page).to have_content(user.tarefas.count) }
    end

    describe "pet" do
      specify{ expect(page).to have_content("Ruirefa") }
    end
  end

  describe "edit" do
    let(:user) { FactoryGirl.create(:user) }
    before do
      login user
      visit edit_user_path(user)
    end

    describe "page" do
      specify{ expect(page).to have_content("Ajustes") }
      specify{ expect(page).to have_title("Ajustes") }
    end

    describe "with valid information" do
      let(:new_name)  { "New Name" }
      let(:new_email) { "new@example.com" }
      before do
        fill_in "Nome",             with: new_name
        fill_in "Email",            with: new_email
        fill_in "Senha",         with: user.password
        fill_in "Confirmar Senha", with: user.password
        click_button "Salvar modificacoes"
      end

      specify{ expect(page).to have_selector('div.alert.alert-success') }
      specify{ expect(page).to have_title("petRefa temporario") }
      specify{ expect(page).to have_link('Logout', href: logout_path) }
      specify { expect(user.reload.name).to  eq new_name }
      specify { expect(user.reload.email).to eq new_email }
    end

    describe "with invalid information" do
      before { click_button "Salvar modificacoes" }

      specify{ expect(page).to have_content("erro") }
    end
  end

  describe "signup page" do
    before { visit cadastro_path }

    specify{ expect(page).to have_content('Cadastro') }
    specify{ expect(page).to have_content('Cadastro') }
  end

  describe "signup" do

    before { visit cadastro_path }

    let(:submit) { "Criar conta!" }

    describe "with invalid information" do
      it "should not create a user" do
        expect { click_button submit }.not_to change(User, :count)
      end
    end

    describe "with valid information" do
      before do
        fill_in "Nome",         with: "Usuario1"
        fill_in "Email",        with: "usuario1@usuarios.com"
        fill_in "Senha",     with: "foobar"
        fill_in "Confirmar senha", with: "foobar"
      end

      it "should create a user" do
        expect { click_button submit }.to change(User, :count).by(1)
      end

      describe "after saving the user" do
        before { click_button submit }
        let(:user) { User.find_by(email: 'usuario1@usuarios.com') }

        specify{ expect(page).to have_link('Logout') }
        specify{ expect(page).to have_title("petRefa temporario") }
        specify{ expect(page).to have_selector('div.alert.alert-success',
                                          text: 'Bem-vindo/a!') }
      end
    end
  end
end
