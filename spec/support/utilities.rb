include ApplicationHelper
def valid_login(user)
  fill_in "Email",    with: user.email
  fill_in "Senha", with: user.password
  click_button "Login"
end

RSpec::Matchers.define :have_error_message do |message|
  match do |page|
    expect(page).to have_selector('div.alert.alert-error', text: message)
  end
end

def login(user, options={})
  if options[:no_capybara]
    # Sign in when not using Capybara.
    remember_token = User.new_remember_token
    cookies[:remember_token] = remember_token
    user.update_attribute(:remember_token, User.digest(remember_token))
  else
    visit login_path
    fill_in "Email",    with: user.email
    fill_in "Senha", with: user.password
    click_button "Login"
  end
end
